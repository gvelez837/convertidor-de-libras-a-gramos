package facci.pm.gemavelez.evaluacionpractica;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    EditText txtconvertir;
    Button btnConvertir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        txtconvertir = (EditText) findViewById(R.id.txtconvertir);
        btnConvertir = (Button) findViewById(R.id.btnConvertir);
        btnConvertir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(MainActivity.this, Activity_convertir.class);
                Bundle bundle = new Bundle();

                bundle.putString("Libras", txtconvertir.getText().toString());

                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
    }
}
